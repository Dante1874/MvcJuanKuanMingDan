﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;

namespace MvcJuanKuanMingDan.Common
{
    public class Utils
    {
        #region 导出Excel
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnStr"></param>
        /// <param name="fileName"></param>
        /// <param name="Response"></param>
        /// <returns></returns>
        public static bool ExportExcel(DataTable dt, List<KeyValue> columns, string fileName)
        {
            bool succeed = true;
            if(dt != null)
            {
                try
                {
                    //var columns = JsonHelper.Deserialize<List<KeyValue>>(columnsStr);
                    for(int j = 0 ; j < columns.Count ; j++)
                    {
                        for(var k = 0 ; k < dt.Columns.Count ; k++)//------把数据库列进行排序
                        {

                            if(columns[j].Value.ToLower().Equals(dt.Columns[k].ColumnName.ToLower()))
                            {
                                dt.Columns[k].ColumnName = columns[j].Key;
                                dt.Columns[k].SetOrdinal(j);//--列排序
                                break;
                            }
                        }
                    }
                    for(var k = dt.Columns.Count - 1 ; k >= 0 ; k--)//-------删除无用的数据表列
                    {
                        var col = columns.Find(x => x.Key.Equals(dt.Columns[k].ColumnName));
                        if(col == null)
                            dt.Columns.Remove(dt.Columns[k]);
                    }



                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet cellSheet = workbook.Worksheets[0];
                    //为单元格添加样式    
                    Aspose.Cells.Style style = workbook.Styles[workbook.Styles.Add()];
                    //设置居中
                    style.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
                    //设置背景颜色

                    style.ForegroundColor = System.Drawing.Color.FromArgb(153, 204, 0);
                    style.Pattern = Aspose.Cells.BackgroundType.Solid;
                    style.Font.IsBold = true;
                    int rowIndex = 0;
                    int colIndex = 0;
                    int colCount = dt.Columns.Count;
                    int rowCount = dt.Rows.Count;
                    //列名的处理
                    for(int i = 0 ; i < colCount ; i++)
                    {
                        cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Columns[i].ColumnName);
                        cellSheet.Cells[rowIndex, colIndex].Style.Font.IsBold = true;
                        cellSheet.Cells[rowIndex, colIndex].Style.Font.Name = "宋体";
                        cellSheet.Cells[rowIndex, colIndex].Style = style;
                        colIndex++;
                    }
                    rowIndex++;
                    for(int i = 0 ; i < rowCount ; i++)
                    {
                        colIndex = 0;
                        for(int j = 0 ; j < colCount ; j++)
                        {
                            if(dt.Columns[j].DataType == typeof(DateTime))
                            {
                                style = cellSheet.Cells[rowIndex, colIndex].GetStyle();
                                style.Custom = "yyyy/M/d hh:mm:ss";
                                if(dt.Rows[i][j].ToString() != "")
                                {
                                    var time = DateTime.Parse(dt.Rows[i][j].ToString());
                                    if(time.Hour == 0 && time.Minute == 0 && time.Second == 0 && time.Millisecond == 0)
                                        style.Custom = "yyyy/M/d";
                                }
                                cellSheet.Cells[rowIndex, colIndex].SetStyle(style);
                            }
                            cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j]);
                            colIndex++;
                        }
                        rowIndex++;
                    }
                    cellSheet.AutoFitColumns();
                    workbook.Save(HttpUtility.UrlEncode(string.Format("{0}.xls", fileName), System.Text.Encoding.UTF8), Aspose.Cells.SaveType.OpenInExcel, Aspose.Cells.FileFormatType.Excel2003, HttpContext.Current.Response);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.Close();
                    workbook = null;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                catch(Exception ex)
                {
                    succeed = false;
                }
            }
            return succeed;
        }

        /// <summary>
        /// 根据模板导出excel文件
        /// </summary>
        /// <param name="ds">数据源:Table需设置TableName</param>
        /// <param name="xlsTemplateName">模板文件名称</param>
        /// <param name="exportXlsName">文件导出名称</param>
        public static void ExportExcelForTempl(DataSet ds, string xlsTemplateName, string exportXlsName)
        {
            string template_path = HttpRuntime.AppDomainAppPath + "\\XlsTemplate\\";
            string xlsReportPath = xlsTemplateName;

            WorkbookDesigner designer = new WorkbookDesigner();
            string path = template_path + xlsReportPath;
            designer.Open(path);
            foreach(DataTable dt in ds.Tables)
            {
                designer.SetDataSource(dt);
            }
            //designer.SetDataSource(orderTable);
            //var time = E68Request.DateTimeToUtc(orderModel.orderDate);
            //designer.SetDataSource("curText", formatCurText(orderModel.curText));
            //designer.SetDataSource("totalPrice", orderModel.totalPrice);
            //designer.SetDataSource("orderDate", orderModel == null ? time : orderModel.cusOrderNo);
            //designer.SetDataSource("payCent", invTable.Rows[0]["payCent"].ToString() + "%");
            designer.Process();
            //var sheet = designer.Workbook.Worksheets[0].Workbook.Worksheets[0];
            //int r = 14;
            //for(int i = 0 ; i < mainTable.Rows.Count ; i++)
            //{
            //    if((r + i) % 2 == 1)
            //    {
            //        for(int j = 0 ; j < 10 ; j++)
            //        {
            //            var s = sheet.Cells[r + i, j].GetStyle();
            //            s.ForegroundColor = Color.FromArgb(217, 217, 217);
            //            s.Pattern = BackgroundType.Solid;
            //            sheet.Cells[r + i, j].SetStyle(s);
            //        }
            //    }
            //}
            // designer.Save(Server.UrlEncode(string.Format("PO#{1}_{0}_{2}% payment.xls", invTable.Rows[0]["invNo"].ToString(), orderModel.cusOrderNo, invTable.Rows[0]["payCent"].ToString())).Replace("+", "%20").Replace("%23","#"), SaveType.OpenInExcel, FileFormatType.Excel2003, Response);
            //  designer.Save(Server.UrlPathEncode(string.Format("PO#{1}_{0}_{2}% payment.xls", invTable.Rows[0]["invNo"].ToString(), orderModel.cusOrderNo, invTable.Rows[0]["payCent"].ToString())).Replace("+", "%20").Replace("%23", "#"), SaveType.OpenInExcel, FileFormatType.Excel2003, Response);
            designer.Save(HttpUtility.UrlEncode(string.Format("{0}{1}.xls", exportXlsName, DateTime.Now.ToString("yyyyMMdd")), System.Text.Encoding.UTF8), SaveType.OpenInExcel, FileFormatType.Excel2003, HttpContext.Current.Response);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.Close();
            designer = null;
            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnStr"></param>        
        /// <returns></returns>
        public static MemoryStream ExportExcel(DataTable dt, string columnsStr)
        {
            if(dt != null)
            {
                try
                {
                    var columns = JsonHelper.Deserialize<List<KeyValue>>(columnsStr);
                    for(int j = 0 ; j < columns.Count ; j++)
                    {
                        for(var k = 0 ; k < dt.Columns.Count ; k++)//------把数据库列进行排序
                        {

                            if(columns[j].Value.ToLower().Equals(dt.Columns[k].ColumnName.ToLower()))
                            {
                                dt.Columns[k].ColumnName = columns[j].Key;
                                dt.Columns[k].SetOrdinal(j);//--列排序
                                break;
                            }
                        }
                    }
                    for(var k = dt.Columns.Count - 1 ; k >= 0 ; k--)//-------删除无用的数据表列
                    {
                        var col = columns.Find(x => x.Key.Equals(dt.Columns[k].ColumnName));
                        if(col == null)
                            dt.Columns.Remove(dt.Columns[k]);
                    }
                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet cellSheet = workbook.Worksheets[0];
                    //为单元格添加样式    
                    Aspose.Cells.Style style = workbook.Styles[workbook.Styles.Add()];
                    //设置居中
                    style.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
                    //设置背景颜色
                    style.ForegroundColor = System.Drawing.Color.FromArgb(153, 204, 0);
                    style.Pattern = Aspose.Cells.BackgroundType.Solid;
                    style.Font.IsBold = true;
                    int rowIndex = 0;
                    int colIndex = 0;
                    int colCount = dt.Columns.Count;
                    int rowCount = dt.Rows.Count;
                    //列名的处理
                    for(int i = 0 ; i < colCount ; i++)
                    {
                        cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Columns[i].ColumnName);
                        cellSheet.Cells[rowIndex, colIndex].Style.Font.IsBold = true;
                        cellSheet.Cells[rowIndex, colIndex].Style.Font.Name = "宋体";
                        cellSheet.Cells[rowIndex, colIndex].Style = style;
                        colIndex++;
                    }
                    rowIndex++;
                    for(int i = 0 ; i < rowCount ; i++)
                    {
                        colIndex = 0;
                        for(int j = 0 ; j < colCount ; j++)
                        {
                            if(dt.Columns[j].DataType == typeof(DateTime))
                            {
                                style = cellSheet.Cells[rowIndex, colIndex].GetStyle();
                                style.Custom = "yyyy/M/d hh:mm:ss";
                                if(dt.Rows[i][j].ToString() != "")
                                {
                                    var time = DateTime.Parse(dt.Rows[i][j].ToString());
                                    if(time.Hour == 0 && time.Minute == 0 && time.Second == 0 && time.Millisecond == 0)
                                        style.Custom = "yyyy/M/d";
                                }
                                cellSheet.Cells[rowIndex, colIndex].SetStyle(style);
                            }
                            cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j]);
                            colIndex++;
                        }
                        rowIndex++;
                    }
                    cellSheet.AutoFitColumns();
                    var result = workbook.SaveToStream();
                    workbook = null;
                    return result;

                }
                catch(Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// 解析 主键 对应（导出excel 字段与 标题对应）
        /// </summary>
        public class KeyValue
        {
            public string Key
            {
                get;
                set;
            }
            public string Value
            {
                get;
                set;
            }
        }
        #endregion

        public static string HttpGet(int index)
        {
            string url = "http://www.qschou.com/ajax/support/fac6d182-063a-4698-ae37-30b3cd69c707/" + index + "/2457";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            request.Accept = "application/json, text/javascript, */*; q=0.01";
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36";
            Cookie c1 = new Cookie("PHPSESSID", HttpUtility.UrlEncode("i3hcct9a95lf943usc29jm8nv35i0vps"), "/", "qschou.com");
            Cookie c2 = new Cookie("aliyungf_tc", HttpUtility.UrlEncode("AQAAADkqqyq3EgEAUnGOPbDbfXdE/qPR"), "/", "qschou.com");
            Cookie c3 = new Cookie("Hm_lvt_65dfcf8f1948f7203dd3fb620de01083", HttpUtility.UrlEncode("1465983898,1466043031,1466053489/qPR"), "/", "qschou.com");
            Cookie c4 = new Cookie("Hm_lpvt_65dfcf8f1948f7203dd3fb620de01083", HttpUtility.UrlEncode("1466053522"), "/", "qschou.com");
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(c1);
            request.CookieContainer.Add(c2);
            request.CookieContainer.Add(c3);
            request.CookieContainer.Add(c4);
            request.Referer = "http://www.qschou.com/project/fac6d182-063a-4698-ae37-30b3cd69c707?uuid=bbd696d6-5ba7-41d3-91d9-380bd5df190b&platform=wechat&shareto=1&from=timeline&isappinstalled=0";
            SetHeaderValue(request.Headers, "Host", "www.qschou.com");
            SetHeaderValue(request.Headers, "Connection", "keep-alive");
            request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();
            return retString;
        }

        public static void SetHeaderValue(WebHeaderCollection header, string name, string value)
        {
            var property = typeof(WebHeaderCollection).GetProperty("InnerCollection",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if(property != null)
            {
                var collection = property.GetValue(header, null) as NameValueCollection;
                collection[name] = value;
            }
        }

        #region DataTable 与 IList 互转
        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach(T item in list)
            {
                DataRow row = table.NewRow();

                foreach(PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// DataTable转List ;区分大小写!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static IList<T> ConvertTo<T>(IList<DataRow> rows)
        {
            IList<T> list = null;

            if(rows != null)
            {
                list = new List<T>();

                foreach(DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }

            return list;
        }

        public static IList<T> ConvertTo<T>(DataTable table)
        {
            if(table == null)
            {
                return null;
            }

            List<DataRow> rows = new List<DataRow>();

            foreach(DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return ConvertTo<T>(rows);
        }

        public static T CreateItem<T>(DataRow row)
        {
            T obj = default(T);
            if(row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach(DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                    }
                    catch
                    {
                        // You can log something here
                        throw;
                    }
                }
            }

            return obj;
        }

        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach(PropertyDescriptor prop in properties)
            {
                try
                {
                    table.Columns.Add(prop.Name, prop.PropertyType);
                }
                catch
                {

                    table.Columns.Add(prop.Name, typeof(String));
                }

            }

            return table;
        }

        #endregion
    }
}