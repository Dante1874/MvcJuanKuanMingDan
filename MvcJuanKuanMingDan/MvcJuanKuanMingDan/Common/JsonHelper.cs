﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace MvcJuanKuanMingDan.Common
{
    public class JsonHelper
    {
        /// <summary> 
        /// 对象转JSON 
        /// </summary> 
        /// <param name="obj">对象</param> 
        /// <returns>JSON格式的字符串</returns> 
        public static string ObjectToJSON(object obj)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            try
            {
                byte[] b = Encoding.UTF8.GetBytes(jss.Serialize(obj));
                return Encoding.UTF8.GetString(b);
            }
            catch(Exception ex)
            {

                throw new Exception("JSONHelper.ObjectToJSON(): " + ex.Message);
            }
        }

        /// <summary> 
        /// 数据表转键值对集合
        /// 把DataTable转成 List集合, 存每一行 
        /// 集合中放的是键值对字典,存每一列 
        /// </summary> 
        /// <param name="dt">数据表</param> 
        /// <returns>哈希表数组</returns> 
        public static List<Dictionary<string, object>> DataTableToList(DataTable dt)
        {
            List<Dictionary<string, object>> list
                 = new List<Dictionary<string, object>>();

            foreach(DataRow dr in dt.Rows)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                foreach(DataColumn dc in dt.Columns)
                {
                    dic.Add(dc.ColumnName, dr[dc.ColumnName]);
                }
                list.Add(dic);
            }
            return list;
        }

        /// <summary> 
        /// 数据集转键值对数组字典 
        /// </summary> 
        /// <param name="dataSet">数据集</param> 
        /// <returns>键值对数组字典</returns> 
        public static Dictionary<string, List<Dictionary<string, object>>> DataSetToDic(DataSet ds)
        {
            Dictionary<string, List<Dictionary<string, object>>> result = new Dictionary<string, List<Dictionary<string, object>>>();

            foreach(DataTable dt in ds.Tables)
                result.Add(dt.TableName, DataTableToList(dt));

            return result;
        }

        /// <summary> 
        /// 数据表转JSON 
        /// </summary> 
        /// <param name="dataTable">数据表</param> 
        /// <returns>JSON字符串</returns> 
        public static string DataTableToJSON(DataTable dt)
        {
            var str = ObjectToJSON(DataTableToList(dt));
            #region 解决解析日期类型问题
            str = System.Text.RegularExpressions.Regex.Replace(str, @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt2 = new DateTime(1970, 1, 1);
                dt2 = dt2.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt2 = dt2.ToLocalTime();
                return dt2.ToString("yyyy-MM-dd HH:mm:ss");
            });

            #endregion
            return str;
        }
        /// <summary>
        /// 数据表转JSON 
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="fmt">返回日期格式</param>
        /// <returns>JSON字符串</returns>
        public static string DataTableToJSON(DataTable dt, string fmt)
        {
            var str = ObjectToJSON(DataTableToList(dt));
            #region 解决解析日期类型问题
            str = System.Text.RegularExpressions.Regex.Replace(str, @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt2 = new DateTime(1970, 1, 1);
                dt2 = dt2.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt2 = dt2.ToLocalTime();
                return dt2.ToString(fmt);
            });
            #endregion
            return str;
        }
        /// <summary> 
        /// JSON文本转对象,泛型方法 
        /// </summary> 
        /// <typeparam name="T">类型</typeparam> 
        /// <param name="jsonText">JSON文本</param> 
        /// <returns>指定类型的对象</returns> 
        public static T JSONToObject<T>(string jsonText)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            try
            {
                return jss.Deserialize<T>(jsonText);
            }
            catch(Exception ex)
            {
                throw new Exception("JSONHelper.JSONToObject(): " + ex.Message);
            }
        }

        /// <summary> 
        /// 将JSON文本转换为数据表数据 
        /// </summary> 
        /// <param name="jsonText">JSON文本</param> 
        /// <returns>数据表字典</returns> 
        public static Dictionary<string, List<Dictionary<string, object>>> TablesDataFromJSON(string jsonText)
        {
            return JSONToObject<Dictionary<string, List<Dictionary<string, object>>>>(jsonText);
        }

        /// <summary> 
        /// 将JSON文本转换成数据行 
        /// </summary> 
        /// <param name="jsonText">JSON文本</param> 
        /// <returns>数据行的字典</returns>
        public static Dictionary<string, object> DataRowFromJSON(string jsonText)
        {
            return JSONToObject<Dictionary<string, object>>(jsonText);
        }

        #region 系统自带转换
        /// <summary>
        /// 任何类型转为JSON格式字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize(object obj, string fmt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new JavaScriptSerializer();
            return System.Text.RegularExpressions.Regex.Replace(js.Serialize(obj), @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt = dt.ToLocalTime();
                if(dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToShortDateString();
                else
                    return dt.ToString(fmt);
            });
        }
        /// <summary>
        /// 任何类型转为JSON格式字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize(object obj)
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new JavaScriptSerializer();

            return System.Text.RegularExpressions.Regex.Replace(js.Serialize(obj), @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt = dt.ToLocalTime();
                if(dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToShortDateString();
                else
                    return dt.ToString();
            }).Replace("\\/Date(-62135596800000)\\/", "");
        }

        /// <summary>
        /// 将JSON格式字符串转为置顶类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string json)
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize<T>(json);
        }
        public static T Deserialize<T>(T t, string json)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dic = jss.Deserialize<Dictionary<string, object>>(json);
            Type type = typeof(T);
            T model = t;
            if(dic.Count > 0)
            {
                foreach(var drow in dic)
                {
                    foreach(PropertyInfo pi in type.GetProperties())
                    {
                        try
                        {
                            if(pi.Name == drow.Key)
                            {
                                if(pi.PropertyType.Name.ToLower() != "string" && string.IsNullOrEmpty(drow.Value.ToString()))
                                {
                                    break;
                                }
                                pi.SetValue(model, Convert.ChangeType(drow.Value, pi.PropertyType), null);
                            }
                        }
                        catch(Exception ex)
                        {
                            continue;
                        }

                    }
                }
            }
            return model;

        }


        public static List<T> DeserializeList<T>(string json)
        {

            var result = new List<T>();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Type type = typeof(T);

            ArrayList dic = jss.Deserialize<ArrayList>(json);
            foreach(Dictionary<string, object> drowlist in dic)
            {
                T obj = System.Activator.CreateInstance<T>();
                foreach(var drow in drowlist)
                {

                    foreach(PropertyInfo pi in type.GetProperties())
                    {
                        try
                        {
                            if(pi.Name.ToLower().Equals(drow.Key.ToLower()))
                            {
                                if(!(pi.PropertyType.Name.ToLower() != "string" && string.IsNullOrEmpty(drow.Value.ToString())))
                                    pi.SetValue(obj, Convert.ChangeType(drow.Value, pi.PropertyType), null);
                                break;
                            }
                        }
                        catch(Exception ex)
                        {
                            continue;
                        }

                    }
                }
                result.Add(obj);
            }
            return result;
        }

        #endregion


        #region DataTableתJson

        /// <summary>    
        /// DataTableתJson    
        /// </summary>    
        /// <param name="dtb"></param>    
        /// <returns></returns>  
        public static string Dtb2Json(DataTable dtb)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ArrayList dic = new ArrayList();
            foreach(DataRow row in dtb.Rows)
            {
                Dictionary<string, object> drow = new Dictionary<string, object>();
                foreach(DataColumn col in dtb.Columns)
                {
                    drow.Add(col.ColumnName, row[col.ColumnName]);
                }
                dic.Add(drow);
            }
            string str = jss.Serialize(dic);
            str = Regex.Replace(str, @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt = dt.ToLocalTime();
                if(dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToString("yyyy-MM-dd");
                else
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
            });
            return str;
        }
        /// <summary>    
        /// DataTableתJson    
        /// </summary>    
        /// <param name="dtb"></param>    
        /// <returns></returns>  
        public static ArrayList Dtb2JsonArray(DataTable dtb)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ArrayList dic = new ArrayList();
            foreach(DataRow row in dtb.Rows)
            {
                Dictionary<string, object> drow = new Dictionary<string, object>();
                foreach(DataColumn col in dtb.Columns)
                {
                    drow.Add(col.ColumnName, row[col.ColumnName]);
                }
                dic.Add(drow);
            }
            return dic;
        }
        /// <summary>    
        /// DataTable 转换成数组，字段筛选
        /// </summary>    
        /// <param name="dtb"></param>    
        /// <param name="fileList"></param>
        /// <returns></returns>  
        public static ArrayList Dtb2JsonArray(DataTable dtb, string fileList)
        {
            string[] list = fileList.Split(',');
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ArrayList dic = new ArrayList();
            foreach(DataRow row in dtb.Rows)
            {
                Dictionary<string, object> drow = new Dictionary<string, object>();
                foreach(DataColumn col in dtb.Columns)
                {

                    if(((IList)list).Contains(col.ColumnName))
                        drow.Add(col.ColumnName, row[col.ColumnName]);
                }
                dic.Add(drow);
            }
            return dic;
        }
        /// <summary>
        /// DataTableתJson
        /// </summary>
        /// <param name="dtb"></param>
        /// <param name="fileList"></param>
        /// <returns></returns>
        public static string Dtb2Json(DataTable dtb, string fileList)
        {
            string[] list = fileList.Split(',');
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ArrayList dic = new ArrayList();
            foreach(DataRow row in dtb.Rows)
            {
                Dictionary<string, object> drow = new Dictionary<string, object>();
                foreach(DataColumn col in dtb.Columns)
                {

                    if(((IList)list).Contains(col.ColumnName))
                        drow.Add(col.ColumnName, row[col.ColumnName]);
                }
                dic.Add(drow);
            }
            //Dictionary<string, object> result = new Dictionary<string, object>();
            //result.Add("rows", dic);
            //result.Add("total", dtb.Rows.Count);
            string str = jss.Serialize(dic);
            str = Regex.Replace(str, @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt = dt.ToLocalTime();
                if(dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToString("yyyy-MM-dd");
                else
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
            });
            return str;
        }
        #endregion

        #region JsonתDataTable

        /// <summary>    
        /// JsonתDataTable    
        /// </summary>    
        /// <param name="json"></param>    
        /// <returns></returns>
        public static DataTable Json2Dtb(string json)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ArrayList dic = jss.Deserialize<ArrayList>(json);
            DataTable dtb = new DataTable();
            if(dic.Count > 0)
            {
                foreach(Dictionary<string, object> drow in dic)
                {
                    if(dtb.Columns.Count == 0)
                    {
                        foreach(string key in drow.Keys)
                        {
                            dtb.Columns.Add(key);//, drow[key]== null ? typeof(string): drow[key].GetType());
                        }
                    }
                    DataRow row = dtb.NewRow();
                    foreach(string key in drow.Keys)
                    {
                        row[key] = drow[key];
                    }
                    dtb.Rows.Add(row);
                }
            }
            return dtb;
        }
        #endregion
    }
}