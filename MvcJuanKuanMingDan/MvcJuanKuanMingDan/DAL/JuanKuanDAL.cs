﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Linq;
namespace MvcJuanKuanMingDan.DAL
{
    public class JuanKuanDAL
    {
        private static DataTable GetJuanKuanMingDanTable(int PageIndex, int PageSize, string Name, out  int RowCount)
        {
            RowCount = 0;
            DataSet ds = new DataSet();
            using(SqlConnection connection = new SqlConnection(SQLHelper.connstr))
            {
                //设置参数
                SqlParameter[] parms = new SqlParameter[] { 
                    new SqlParameter(){ ParameterName="@PageIndex",SqlDbType=SqlDbType.Int,Value=PageIndex},
                    new SqlParameter(){ ParameterName="@PageSize",SqlDbType=SqlDbType.Int,Value=PageSize},
                    new SqlParameter(){ ParameterName="@Name",SqlDbType=SqlDbType.NVarChar,Value=Name},
                    new SqlParameter(){ ParameterName="@RowCount",SqlDbType=SqlDbType.Int,Direction=ParameterDirection.Output,Value=RowCount}
                };
                SqlCommand cmd = BuildQueryCommand(connection, "GetUsersPage", parms);
                //cmd.Parameters.AddRange(pars);
                //获取数据
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);
                //关闭连接
                connection.Close();
                RowCount = (int)cmd.Parameters["@RowCount"].Value;
                return ds.Tables[0];
            }
        }

        /// <summary>
        /// 构建 SqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand</returns>
        private static SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach(SqlParameter parameter in parameters)
            {
                if(parameter != null)
                {
                    // 检查未分配值的输出参数,将其分配以DBNull.Value.
                    if((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                        (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    command.Parameters.Add(parameter);
                }
            }

            return command;
        }

        public static IList<User> GetJuanKuanMingDanList(int PageIndex, int PageSize, string Name, out  int RowCount)
        {
            DataTable userTable = GetJuanKuanMingDanTable(PageIndex, PageSize, Name, out RowCount);
            return Common.Utils.ConvertTo<User>(userTable);
        }

        /// <summary>
        /// 轻松筹名单导入
        /// </summary>
        /// <param name="dt"></param>
        public static void SqlBulkCopyByDatatable(DataTable dt)
        {
            using(SqlConnection conn = new SqlConnection(SQLHelper.connstr))
            {
                using(SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(SQLHelper.connstr, SqlBulkCopyOptions.UseInternalTransaction))
                {
                    try
                    {
                        sqlbulkcopy.DestinationTableName = "Users";
                        sqlbulkcopy.ColumnMappings.Add("id", "QingSongChouID");
                        sqlbulkcopy.ColumnMappings.Add("name", "Name");
                        sqlbulkcopy.ColumnMappings.Add("monery", "Monery");
                        sqlbulkcopy.ColumnMappings.Add("comment", "Description");
                        //for(int i = 0 ; i < dt.Columns.Count ; i++)
                        //{
                        //    sqlbulkcopy.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                        //}
                        sqlbulkcopy.WriteToServer(dt);
                    }
                    catch(System.Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        public static void DeleteQingSongChouUser()
        {
            DAL.SQLHelper.ExecuteNonQuery("delete Users where QingSongChouID is not null");
        }
    }
}