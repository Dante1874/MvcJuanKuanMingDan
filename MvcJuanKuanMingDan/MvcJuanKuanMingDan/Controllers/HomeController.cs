﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcJuanKuanMingDan.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //int rowCount = 0;
            //var list = DAL.JuanKuanDAL.GetJuanKuanMingDanList(1, 100, "", out rowCount);
            return View();
        }

        public ActionResult ExportQingSongChou(string key)
        {
            if (key == "1874")
            {
                List<Info> list = new List<Info>();
                string str = "";
                string result = "";
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                Info obj = null;
                int i = 1;
                while (1 == 1)
                {
                    result = Common.Utils.HttpGet(i);
                    obj = js.Deserialize<Info>(result);
                    if (obj.data.Count > 0)
                    {
                        list.Add(obj);
                        str += result;
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("id");
                dt.Columns.Add("name");
                dt.Columns.Add("monery");
                dt.Columns.Add("comment");
                dt.Columns.Add("huifu");
                DataRow dr = null;
                foreach (var item in list)
                {
                    foreach (var item2 in item.data)
                    {
                        dr = dt.NewRow();
                        dr["id"] = item2.id;
                        dr["name"] = item2.user.name;
                        dr["monery"] = item2.moneyTotal;
                        dr["comment"] = item2.words;
                        dr["huifu"] = getComment(item2.comment);
                        dt.Rows.Add(dr);
                    }
                }
                DAL.JuanKuanDAL.DeleteQingSongChouUser();
                DAL.JuanKuanDAL.SqlBulkCopyByDatatable(dt);
                var columns = new List<Common.Utils.KeyValue>(){
               new Common.Utils.KeyValue(){ Key="名称",Value="name"},
               new Common.Utils.KeyValue(){ Key="募捐金额(元)",Value="monery"},
               new Common.Utils.KeyValue(){ Key="留言",Value="comment"},
               new Common.Utils.KeyValue(){ Key="回复",Value="huifu"}
           };
                Common.Utils.ExportExcel(dt, columns, DateTime.Now.ToString("yyyyMMddhhmmss"));
                return View();
            }
            else
            {
                return new HttpNotFoundResult();
            }

        }

        #region 获取轻松筹名单

        private string getComment(List<Info.dataInfo.commentInfo> list)
        {
            string str = "";
            foreach (var item in list)
            {
                str += string.Format("{0}:{1}", item.user.name, item.content) + ",";
            }
            return str.TrimEnd(',');
        }

        public class Info
        {
            public List<dataInfo> data
            {
                get;
                set;
            }
            public class dataInfo
            {
                public string words
                {
                    get;
                    set;
                }
                public int id
                {
                    get;
                    set;
                }
                public string type
                {
                    get;
                    set;
                }
                public float moneyTotal
                {
                    get;
                    set;
                }
                public string want
                {
                    get;
                    set;
                }
                public string readableCreated
                {
                    get;
                    set;
                }
                public string created
                {
                    get;
                    set;
                }
                public string hideComments
                {
                    get;
                    set;
                }
                public userInfo user
                {
                    get;
                    set;
                }
                public List<commentInfo> comment
                {
                    get;
                    set;
                }
                public class commentInfo
                {
                    public string content
                    {
                        get;
                        set;
                    }
                    public int id
                    {
                        get;
                        set;
                    }
                    public userInfo user
                    {
                        get;
                        set;
                    }
                }

                public class userInfo
                {
                    public string uuid
                    {
                        get;
                        set;
                    }
                    public string name
                    {
                        get;
                        set;
                    }
                    public string avatar
                    {
                        get;
                        set;
                    }
                    public string url
                    {
                        get;
                        set;
                    }
                    public int rank
                    {
                        get;
                        set;
                    }
                }
            }
            public int result
            {
                get;
                set;
            }
            public string msg
            {
                get;
                set;
            }
            public string jsonp
            {
                get;
                set;
            }
        }

        #endregion

        public JsonResult GetUsersPage(int PageIndex = 1, int PageSize = 10, string Name = "")
        {
            int itemCount = 0;
            var list = DAL.JuanKuanDAL.GetJuanKuanMingDanList(PageIndex, PageSize, Name, out itemCount).ToList();
            return Json(new
            {
                data = list.ConvertAll<object>(x => new
                {
                    ID = x.ID,
                    RowNumber = x.RowNumber,
                    Name = x.Name,
                    Monery = x.Monery.ToString("F2"),
                    Description = x.Description
                }),
                rowCount = itemCount
            }, "text/html", JsonRequestBehavior.DenyGet);

        }

        public JsonResult GetChePage(int PageIndex = 1, int PageSize = 10, string Name = "")
        {
            int itemCount = 0;
            var list = DAL.CheDAL.GetCheLiangXinXiList(PageIndex, PageSize, Name, out itemCount).ToList();
            return Json(new
            {
                data = list.ConvertAll<object>(x => new
                {
                    ID = x.ID,
                    RowNumber = x.RowNumber,
                    Name = x.Name,
                    yeZhuDanWei = x.yeZhuDanWei,
                    Description = x.Description
                }),
                rowCount = itemCount
            }, "text/html", JsonRequestBehavior.DenyGet);

        }
        public ActionResult CheList()
        {
            return View();
        }

        public ActionResult CheAdd()
        {
            return View();
        }
        public JsonResult AddChe(Che obj)
        {
            int result = -1;
            result = DAL.CheDAL.AddChe(obj);
            return Json(new { status = result }, JsonRequestBehavior.DenyGet);
        }
    }
}
